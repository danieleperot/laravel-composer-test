# Laravel Composer Test

A Laravel Docker environment with Composer.

Built with `PHP Alpine`.

*This is just a base image that could be use in a CI / CD with Docker tool*

It comes with `PHP 7.2`, `PHP 7.3` `PHP 7.4` and `PHP 8.0` images.

Example with `Gitlab CI/CD` using `php 7.3`:
```yml
php-7.3:
  stage: test
  image: dperot/laravel-composer-test:php-7.3
  before_script:
    - cp .env.testing .env
    - composer install --no-progress --no-interaction
  script:
    - php vendor/bin/phpunit --colors
```

